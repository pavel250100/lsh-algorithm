import time

def correlation(set_a,set_b):
    unions = len(set_a.union(set_b))
    intersections = len(set_a.intersection(set_b))
    return 1. * intersections / unions

t0 = time.time()
f = open("dataset/all_articles.txt")

lst = []
lst_id = []
for i in f:
    i = i.split(" ",1)
    lst_id.append(i[0])
    
    words = i[1].split(" ")
    tmp_word_list = []
    for i in range(0, len(words) - 2):
        shingle = words[i] + " " + words[i + 1] + " " + words[i + 2]
        tmp_word_list.append(shingle)
    # print(tmp_word_list)
    # input()
    lst.append(tmp_word_list)

len_txt = len(lst_id)

for i in range(0,len_txt):
    for j in range(i+1, len_txt):
        if correlation(set(lst[i]), set(lst[j])) > 0.9:
            print(lst_id[i], lst_id[j])
t1 = time.time()

print(str(t1-t0)+"s")