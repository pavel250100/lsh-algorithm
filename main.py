import functions_library as lib
import time

class LocalSensitivityHasher:
    def __init__(self, filepath, shingle_size):
        self.data = lib.load_data(filepath)
        self.shingles = lib.get_shingles_map(self.data, shingle_size)
        self.signature_matrix = None
        self.shingle_vocabulary = None
        self.process_done = True

    # vocabulary means the union of all shingles
    def __init_vocabulary(self):
        self.shingle_vocabulary = set.union(*self.shingles.values())

    # private function for generating signature matrix
    def __init_signature_matrix(self, n):
        print("Initializing signature matrix...")
        self.__init_vocabulary()
        shingle_map = dict(zip(list(self.shingle_vocabulary), range(len(self.shingle_vocabulary))))
        start = time.time()
        self.signature_matrix = lib.generate_signature_matrix(self.shingles.values(), shingle_map, n)
        print(f"Time taken for initializing SIGNATURE MATRIX: {(time.time() - start):.4f} seconds\n")

    # the fastest function which finds similar articles using hash-buckets, bands
    def find_lsh_based(self, n, b, r):
        start_overall = time.time()
        self.__init_signature_matrix(n)
        start = time.time()
        print("Finding pairs using LSH...")
        candidates = lib.lsh_pairs(self.signature_matrix, self.data.keys(), b, r)
        end_time = time.time()
        print(f"Time taken for LSH algorithm: {(end_time - start):.4f} seconds\n")
        print(f"Time taken overall: {(end_time - start_overall):.4f} seconds")
        return candidates

    # function finds similar articles based on the shingles
    def find_shingles_based(self, jaccard_threshold):
        start_overall = time.time()
        print("Finding pairs using shingles...")
        start = time.time()
        candidates = lib.candidate_pairs(lib.true_sim_scores(self.shingles), jaccard_threshold)
        end_time = time.time()
        print(f"Time taken for SHINGLES based algorithm: {(end_time - start):.4f} seconds\n")
        print(f"Time taken overall: {(end_time - start_overall):.4f} seconds")
        return candidates

    # function finds similar articles based on the generated signature matrix by comparing columns
    def find_signature_matrix_based(self, n, jaccard_threshold):
        start_overall = time.time()
        self.__init_signature_matrix(n)
        start = time.time()
        print("Finding pair based on signature matrix columns...")
        candidates = lib.candidate_pairs(lib.signature_sim_scores(self.signature_matrix,
                                                                  self.data.keys()
                                                                  ), jaccard_threshold)
        end_time = time.time()
        print(f"Time taken for SIGNATURE MATRIX based algorithm: {(end_time - start):.4f} seconds\n")
        print(f"Time taken overall: {(end_time - start_overall):.4f} seconds")
        return candidates


if __name__ == "__main__":
    p = 0.9
    r = 12
    b = int(p**(-1 * r))
    n = r * b
    print(f"r={r} b={b} p={p}")
    lsh = LocalSensitivityHasher('./dataset/all_articles.txt', 3)
    similar_articles = lsh.find_lsh_based(n, b, r)
    lib.write_result(similar_articles, 'result.txt')
    lib.validate(similar_articles)
