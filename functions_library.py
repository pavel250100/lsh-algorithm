import itertools
import numpy as np
import collections
import time

def load_data(filepath: str):
    dataset = {}
    for line in open(filepath).readlines():
        id_to_article = line.split(' ', 1)
        dataset[id_to_article[0]] = id_to_article[1][:-1]
    return dataset

def get_true_results(filepath):
    result = []
    for line in open(filepath).readlines():
        ids = line.split(' ')
        result.append(tuple([ids[0], ids[1][:-1]]))
    return result

def get_shingles(article: str, shingle_size: int) -> set:
    shingles = set()
    words = tuple(article.split(' '))
    for i in range(0, len(words) - shingle_size + 1):
        shingles.add(words[i: i + shingle_size])
    return shingles

def jaccard_sim(set1: set, set2: set):
    return 1. * len(set1.intersection(set2)) / len(set1.union(set2))

def true_sim_scores(doc):
    pairs = []
    similarities = []
    indecies = doc.keys()
    for x1, x2 in itertools.combinations(zip(indecies, doc.values()), 2):
        pairs.append((x1[0], x2[0]))
        similarities.append(jaccard_sim(x1[1], x2[1]))
    return dict(zip(pairs, similarities))

def signature_sim_scores(sig_mat, keys):
    cols = sig_mat.T
    pair_labels = []
    pair_sims = []
    for (i, col1), (j, col2) in itertools.combinations(zip(keys, cols), 2):
        pair_labels.append((i, j))
        pair_sims.append(np.mean(col1 == col2))
    return dict(zip(pair_labels, pair_sims))


def candidate_pairs(score_dict, threshold):
    return set(pair for pair, scr in score_dict.items() if scr >= threshold)


def lsh_pairs(signature_matrix, keys, b, r):
    n = signature_matrix.shape[0]
    d = list(keys)
    assert(n == b*r)
    hash_buckets = collections.defaultdict(set)
    bands = np.array_split(signature_matrix, b, axis=0)
    for i, band in enumerate(bands):
        for j in range(len(d)):
            band_id = tuple(list(band[:, j])+[str(i)])
            hash_buckets[band_id].add(d[j])
    candidate_pairs = set()
    for bucket in hash_buckets.values():
        if len(bucket) > 1:
            for pair in itertools.combinations(bucket, 2):
                candidate_pairs.add(pair)
    return candidate_pairs


def get_shingles_map(data, shingle_size):
    print("Initializing shingles for articles...")
    start_time = time.time()
    shingles = {}
    for key, article in data.items():
        shingles[key] = get_shingles(article, shingle_size)
    print(f"Time taken for initializing SHINGLES: {(time.time() - start_time):.4f} seconds\n")
    return shingles

def generate_signature_matrix(article_shingles, shingle_dict, n):
    def permute_row(row, params, random_limit):
        return (params @ np.array([1, row])) % random_limit
    random_limit = len(shingle_dict)
    permutations = np.random.randint(random_limit, size=[n, 2])
    signature_matrix = np.full((n, len(article_shingles)), np.inf)
    for i, shingles in enumerate(article_shingles):
        for shingle in shingles:
            row = shingle_dict[shingle]
            col = permute_row(row, permutations, random_limit)
            signature_matrix[:, i] = np.minimum(signature_matrix[:, i], col)
    return signature_matrix.astype(int)

# function for writing pair in result.txt
def write_result(pairs, filepath):
    file = open(filepath, 'w')
    for pair in pairs:
        file.write(f"{pair[0]} {pair[1]}\n")

# compare results with linear search, results of which are in true_result.txt
def validate(pairs):
    true_results = get_true_results('./true_result.txt')
    print("~~~~~~~~~~~~~~RESULTS~~~~~~~~~~~~~~")
    cnt_found = 0
    for item in true_results:
        for tuple in pairs:
            if (item[0] == tuple[1] or item[0] == tuple[0]) and (item[1] == tuple[0] or item[1] == tuple[1]):
                cnt_found += 1
    print(f"Found: {cnt_found}\n"
          f"Not found: {len(true_results) - cnt_found}\n"
          f"Total number of true pairs: {len(true_results)}\n"
          f"Accuracy: {(1.0 * cnt_found / len(true_results) * 100.0):.3f}%")