# FTEC4005 Course Project Task 1: Find Similar Articles with LSH
## 1. Background
We discussed searching for "similar" items in class. This kind of problem exists widely in real life, such as checking mirror pages on the Internet, checking duplicate documents, and can also be used to quickly find similar users or similar items in the business system. 

In this task, given a data set containing text, it is required to apply LSH to quickly find **all similar text pairs** within it.  **Similar pair** is defined as two text strings with **Jaccard similarity greater than 0.9** after word-based 3-shingle. 

For example, given two strings "I eat an apple today." and "I eat an orange today." (without quotes). They become {"I eat an", "eat an apple", "an apple today."} and {"I eat an", "eat an orange", "an orange today."} after word-based 3-shingle. The Jaccard similarity of the two sets is 0.2. 

## 2. Data Set Information

#### This data set contains one file:
1. all_articles.txt
	
	- There are $10000$ articles in this dataset. Each line of the dataset is an article.
	- Each line begins with t_id (note that the t_id is not in order), followed by the specific content of the text, Format is as follows (<article_content> is a string, and you can look at the data set for more information):
```
t132 <article_content>
t212 <article_content>
...
```
2. test_articles.txt
	
	- This is a small data set for you to debug. There are $100$ articles in this test dataset. Each line of the dataset is an article. In test_ans.txt, we show which pairs on the test dataset are similar. You can refine your code on small datasets and then run it on all articles dataset.
	- This file has the same format as all_articles.txt. 
3. test_ans.txt
   - This shows which articles are similar to the test article dataset. The Format is as follows:
```
t1088 t5015
t1297 t4638
t1768 t5248
...
```
4. linear_search.py
   - This code implements a simple linear lookup (every two different texts are compared) for your reference. Run it and it prints out which articles are similar and run time. You can also refer to this code to check whether your answer is correct.

## 3. Goal

You should implement an algorithm to find all similar articles in all_articles.txt. Save **the all similar pairs** you find as **result.txt** (format is the same as test_ans.txt). These results need not be in order. 

In your final report, you should **describe your method in detail** and show the following points:

- The **running time** of your algorithm (report the **time spent processing the data** and the **time spent searching**, respectively). 
- The **running time of linear_search** on your machine (this is to compare your acceleration rate).
- The score of this task will depend on the speed-up of your method over the baseline method and the precision of the pairs you found.
- You **must** use an LSH based approach to solve the problem.

PS: Remember to upload your **code** (with the necessary comments). We'll check and run your code (make sure your code runs and outputs the run time and results).  
